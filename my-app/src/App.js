import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

const movies = [
  {
    id: '1',
    title: 'Oceans 8',
    category: 'Comedy',
    likes: 4,
    dislikes: 1
  }, {
    id: '2',
    title: 'Midnight Sun',
    category: 'Comedy',
    likes: 2,
    dislikes: 0
  }, {
    id: '3',
    title: 'Les indestructibles 2',
    category: 'Animation',
    likes: 3,
    dislikes: 1
  }, {
    id: '4',
    title: 'Sans un bruit',
    category: 'Thriller',
    likes: 6,
    dislikes: 6
  }, {
    id: '5',
    title: 'Creed II',
    category: 'Drame',
    likes: 16,
    dislikes: 2
  }, {
    id: '6',
    title: 'Pulp Fiction',
    category: 'Thriller',
    likes: 11,
    dislikes: 3
  }, {
    id: '7',
    title: 'Pulp Fiction',
    category: 'Thriller',
    likes: 12333,
    dislikes: 32
  }, {
    id: '8',
    title: 'Seven',
    category: 'Thriller',
    likes: 2,
    dislikes: 1
  }, {
    id: '9',
    title: 'Inception',
    category: 'Thriller',
    likes: 2,
    dislikes: 1
  }, {
    id: '10',
    title: 'Gone Girl',
    category: 'Thriller',
    likes: 22,
    dislikes: 12
  },
]

var likeBar = fillLikeBar(movies);

function fillLikeBar(movies) {
  var tab = [];

  for (var i = 0; i < movies.length; i++) {
    tab[i] = (movies[i].likes * 100) /(movies[i].likes + movies[i].dislikes);
  }
  return tab;
}

var element = movies.map((movies) =>
<div id={movies.id} className={'card ' + movies.category}>
<div class="card-body">
	<h2 class="card-title">{movies.title}</h2>
	<p class="card-text">{movies.category}</p>
  <div class="progress-bar" style={{width:likeBar[movies.id] +'%'}} role="progressbar" aria-valuenow={likeBar[movies.id]} aria-valuemin="0" aria-valuemax="100"></div>
</div>
</div>);



ReactDOM.render(
	element,
	document.getElementById('test')
);
